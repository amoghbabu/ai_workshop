
import pandas as pd

import numpy as np

from sklearn.tree import DecisionTreeClassifier

from sklearn.model_selection import train_test_split

from sklearn.metrics import accuracy_score

from sklearn.tree import export_graphviz

##################### Read CSV Files ###########################

df = pd.read_csv(r"C:\Users\adminpc\Desktop\wine.csv")


######################### Feature Selction ####################################

Features = df.iloc[:,0:13] 
Class = df.iloc[:,-1]

np.random.seed(123)

####################### DT Model using GINI #################################

X_train, X_test, y_train, y_test = train_test_split(Features, Class, test_size = 0.3, random_state = 100)
# Show the results of the split
print("Training set has {} samples.".format(X_train.shape[0]))
print("Testing set has {} samples.".format(X_test.shape[0]))


DTModel1 = DecisionTreeClassifier(criterion = "gini",random_state = 100,max_depth=3, min_samples_leaf=5) 
  
DTModel1.fit(X_train, y_train)

results = DTModel1.predict(X_test)

Accuracy_gini = accuracy_score(y_test,results)*100

####################### DT Model using ENTROPY #################################

DTModel2 = DecisionTreeClassifier(criterion = "entropy",random_state = 100,max_depth=3, min_samples_leaf=5) 
  
DTModel2.fit(X_train, y_train)

results2 = DTModel2.predict(X_test)

Accuracy_entropy = accuracy_score(y_test,results2)*100

print(Accuracy_gini,Accuracy_entropy)

################### Model Graphs ################################################

dotfile = open("E:/dtree3.dot", 'w')

export_graphviz(DTModel1, out_file = dotfile, feature_names = Features.columns)

dotfile.close()

dotfile = open("E:/dtree4.dot", 'w')

export_graphviz(DTModel2, out_file = dotfile, feature_names = Features.columns)

dotfile.close()

#################################### To Generate Graphs ################################################
"""
Open the .dot file in Text Editor

copy the Graph codes

Paste the Codes @ http://webgraphviz.com/

"""
